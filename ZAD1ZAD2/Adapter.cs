﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1ZAD2
{
    class Adapter
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> list = dataset.GetData();
            int elementsInList = list.Count;
            double[][] matrix = new double[elementsInList][];
            for (int i = 0; i < elementsInList; ++i)
            {
                matrix[i] = new double[list[i].Count];
            }
            for (int i = 0; i < elementsInList; ++i)
            {
                for (int j = 0; j < list[i].Count; ++j)
                {
                    matrix[i][j] = list[i][j];
                }
            }
            return matrix;

        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }

    }
}
