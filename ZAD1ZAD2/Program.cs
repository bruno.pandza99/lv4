﻿using System;
using System.Collections.Generic;

namespace ZAD1ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("C:\\Users\\gospodinbruno\\source\\repos\\LV4RPPOON\\ZAD1\\CSVfileLV4.csv");
            Adapter adapterForAnalyzer = new Adapter(new Analyzer3rdParty());
            foreach (List<double> numbers in data.GetData())
            {
                foreach (double number in numbers)
                {
                    Console.Write(number + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            double[] rowAverages = adapterForAnalyzer.CalculateAveragePerRow(data);
            double[] columnAverages = adapterForAnalyzer.CalculateAveragePerColumn(data);
           
            foreach (double row in rowAverages)
            {
                Console.WriteLine(Math.Round(row, 1));
            }
            Console.WriteLine();

            foreach (double column in columnAverages)
            {
                Console.WriteLine(Math.Round(column, 1));
            }



        }
    }
}
